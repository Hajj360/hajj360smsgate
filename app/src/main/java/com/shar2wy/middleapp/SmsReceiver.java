package com.shar2wy.middleapp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.Toast;

import com.shar2wy.middleapp.network.ApiClient;
import com.shar2wy.middleapp.network.ApiService;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by shar2wy
 * on 8/2/18.
 * Description: description goes here
 */
public class SmsReceiver extends BroadcastReceiver {
    private String TAG = SmsReceiver.class.getSimpleName();

    public SmsReceiver() {

    }

    @Override
    public void onReceive(Context context, Intent intent) {
        // Get the data (SMS data) bound to intent
        Bundle bundle = intent.getExtras();
        SmsMessage[] msgs = null;
        String str = "";
        if (bundle != null) {
            // Retrieve the SMS Messages received
            Object[] pdus = (Object[]) bundle.get("pdus");
            msgs = new SmsMessage[pdus.length];
            // For every SMS message received
            for (int i = 0; i < msgs.length; i++) {
                // Convert Object array
                msgs[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
                // Sender's phone number
//                str += "SMS from " + msgs[i].getOriginatingAddress() + " : ";
                str += "SMS from " + msgs[i].getDisplayOriginatingAddress() + " : ";
                // Fetch the text message
                str += msgs[i].getMessageBody().toString();
                // Newline 🙂
                str += "\n";
                handleSms(context, msgs[i].getOriginatingAddress(), msgs[i].getMessageBody().toString());
            }
            // Display the entire SMS Message
            Log.d(TAG, str);
        }
    }

    private void handleSms(final Context context, String number, String message) {

        if (message.isEmpty()) {
            SmsManger.sendSms(context, number, "04df5");
        } else {
            String[] arrs = message.split("-");
            ApiClient
                    .getClient(context)
                    .create(ApiService.class)
                    .askForHelp(arrs[0], Integer.parseInt(arrs[1]), arrs[2])
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(result -> {
                        SmsManger.sendSms(context, number, result.getMessage());
                        Toast.makeText(context, "Success", Toast.LENGTH_SHORT).show();
                    }, throwable -> {
                        throwable.printStackTrace();
                        Toast.makeText(context, "failed", Toast.LENGTH_SHORT).show();
                    });
        }


    }
}