package com.shar2wy.middleapp.network;

import com.shar2wy.middleapp.Response;

import io.reactivex.Single;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ApiService {

    @FormUrlEncoded
    @POST("services/request")
    Single<Response> askForHelp(
            @Field("service_type") String type,
            @Field("user_id") int userId,
            @Field("location") String location
    );

}
