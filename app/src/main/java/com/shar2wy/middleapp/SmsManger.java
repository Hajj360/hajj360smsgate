package com.shar2wy.middleapp;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.telephony.SmsManager;

/**
 * Created by shar2wy
 * on 8/2/18.
 * Description: description goes here
 */
public class SmsManger {

    public static void sendSms(Context mContext, String smsBody) {
        String SMS_SENT = "SMS_SENT";
        String SMS_DELIVERED = "SMS_DELIVERED";

        PendingIntent sentPendingIntent = PendingIntent.getBroadcast(mContext, 0, new Intent(SMS_SENT), 0);
        PendingIntent deliveredPendingIntent = PendingIntent.getBroadcast(mContext, 0, new Intent(SMS_DELIVERED), 0);


        // Get the default instance of SmsManager
        SmsManager smsManager = SmsManager.getDefault();
    // Send a text based SMS
        smsManager.sendTextMessage(Constants.PHONE_NUMBER, null, smsBody, sentPendingIntent, deliveredPendingIntent);
    }

    public static void sendSms(Context mContext,String number, String smsBody) {
        String SMS_SENT = "SMS_SENT";
        String SMS_DELIVERED = "SMS_DELIVERED";

        PendingIntent sentPendingIntent = PendingIntent.getBroadcast(mContext, 0, new Intent(SMS_SENT), 0);
        PendingIntent deliveredPendingIntent = PendingIntent.getBroadcast(mContext, 0, new Intent(SMS_DELIVERED), 0);

        // Get the default instance of SmsManager
        SmsManager smsManager = SmsManager.getDefault();
        // Send a text based SMS
        smsManager.sendTextMessage(number, null, smsBody, sentPendingIntent, deliveredPendingIntent);
    }
}
